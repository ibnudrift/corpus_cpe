<div class="cover" style="background-image: url('<?php echo Yii::app()->baseUrl.'/images/static/'. $this->setting['partner_hero_image']; ?>');">
	<div class="text"><h2><?php echo $this->setting['partner_hero_title'] ?></h2></div>
</div>

<section class="breadcrumb-insides">
	<div class="prelative container">
		<nav aria-label="breadcrumb">
		  <ol class="breadcrumb">
		    <li class="breadcrumb-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/index', 'lang'=>Yii::app()->language)); ?>">Home</a></li>
		    <li class="breadcrumb-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/about', 'lang'=>Yii::app()->language)); ?>"><?php echo $this->setting['partner_hero_title'] ?></a></li>
		  </ol>
		  <div class="back float-right">
		  	<a href="<?php echo CHtml::normalizeUrl(array('/home/index', 'lang'=>Yii::app()->language)); ?>"><span><img src="<?php echo $this->assetBaseurl; ?>arrow-back.png" alt=""></span>BACK TO PREVIOUS PAGE</a>
		  </div>
		</nav>
	</div>
</section>

<section class="part-sec-1">
	<div class="prelative container">
		<div class="row">
			<div class="col-md-60">
				<div class="title">
					<p><?php echo $this->setting['partner_title1'] ?></p>
				</div>
				<div class="image"><img src="<?php echo Yii::app()->baseUrl.'/images/static/'. $this->setting['partner_banner1']; ?>" alt=""></div>
				<div class="our-corp">
					<p><?php echo $this->setting['partner_cooperation_title'] ?></p>
				</div>
				<div class="above-our-corp">
					<?php echo $this->setting['partner_cooperation_info'] ?>
				</div>
			</div>
		</div>
	</div>
</section>
