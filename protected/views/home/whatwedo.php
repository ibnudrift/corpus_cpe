<?php 
$numbs = isset( $_GET['id'] )? intval($_GET['id']) : 1;

$criteria = new CDbCriteria;
$criteria->with = array('description');
$criteria->addCondition('description.language_id = :language_id');
$criteria->params[':language_id'] = $this->languageID;
$criteria->addCondition('t.id = :id');
$criteria->params[':id'] = intval( htmlspecialchars($numbs) );
$model = Service::model()->find($criteria);
?>

<div class="cover whatwedo" style="background-image: url('<?php echo Yii::app()->baseUrl.'/images/static/'. $this->setting['products_hero_image']; ?>');">
  <div class="text">
  	<h2 class="what"><?php echo $this->setting['products_hero_title'] ?></h2>
  	<p><?php echo $this->setting['products_hero_subtitle'] ?></p>
  </div>
</div>

<section class="breadcrumb-insides">
	<div class="prelative container">
		<nav aria-label="breadcrumb">
		  <ol class="breadcrumb">
		    <li class="breadcrumb-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>">Home</a></li>
		    <li class="breadcrumb-item"><a href="<?php echo CHtml::normalizeUrl(array('/home/about')); ?>"><?php echo $this->setting['products_hero_title'] ?></a></li>
		  </ol>
		  <div class="back float-right">
		  	<a href="<?php echo CHtml::normalizeUrl(array('/home/index')); ?>"><span><img src="<?php echo $this->assetBaseurl; ?>arrow-back.png" alt=""></span>BACK TO PREVIOUS PAGE</a>
		  </div>
		</nav>
	</div>
</section>

<section class="what-sec-1 d-none d-sm-block">
	<div class="prelative container">
		<div class="row">
			<div class="col-md-25">
				<div class="box-aneh">
					<div class="image"><img class="img img-fluid" src="<?php echo Yii::app()->baseUrl.'/images/static/'. $this->setting['products2_pictures']; ?>" alt=""></div>
					<div class="bawah-gambar pt-3 d-none d-sm-block">
						<div class="row">
							<div class="col-md-60 col-xs-60 col-60">
								<p class="pb-2"><?php echo $this->setting['products2_picturesbtm_content'] ?></b><br>
								<b>2. British Petroleum</b><br>
								<b>3. Gunvor</b><br>
								<b>4. Hin Leong</b><br>
								<b>5. HinHin</b><br>
								<b>6. Pertamina Group</b><br>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-35 pl-4">
				<div class="content">
					<div class="title">
						<p><?php echo $this->setting['products2_small_title'] ?></p>
					</div>
				</div>
				<div class="content-utama">
					<div class="title">
						<p><?php echo $this->setting['products2_subtitle'] ?></p>
					</div>
					<div class="isi">
						<?php echo $this->setting['products2_content'] ?>
					</div>
					<div class="py-4 d-none d-sm-block"></div>
					<div class="py-1"></div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="what-sec-1 d-block d-sm-none">
	<div class="prelative container">
		<div class="row">
			<div class="col-md-35 pl-4 pb-3">
				<div class="content">
					<div class="title">
						<p><?php echo ucwords( strtolower( $model->description->title ) ) ?></p>
					</div>
				</div>
			</div>
			<div class="col-md-25">
				<div class="box-aneh">
<!--					<div class="image"><img src="--><?php //echo $this->assetBaseurl; ?><!----><?php //echo $model['image'] ?><!--" alt=""></div>-->
					<div class="bawah-gambar px-3 pt-3 d-none d-sm-block">
						<div class="row">
							<div class="col-md-5 col-xs-5 col-5">
								<img src="<?php echo $this->assetBaseurl; ?>logo-bawah-gambar.png" alt="">
							</div>
							<div class="col-md-55 col-xs-55 col-55">
								<p>Take the next steps with Corpus Capital, come contact us and talk to our consultants - let’s get your finance growing.</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="what-sec-2">
	<div class="prelative container">
		<div class="row">
			<div class="col-md-25">
			</div>
			<div class="col-md-35 pl-4">
				<div class="row">
					<div class="col-md-60">
						<div class="box-content pt-4">
							<div class="title pt-2 pb-3 mb-3">
								<p><?php echo $this->setting['products2_specification'] ?></p>
							</div>
							<div class="image"><img src="<?php echo Yii::app()->baseUrl.'/images/static/'. $this->setting['products2_specification_banner']; ?>" alt=""></div>
						</div>
					</div>
				</div>
				
			</div>
		</div>
	</div>
</section>

<section class="what-sec-3">
	<div class="prelative container">
		<div class="title">
			<p><?php echo $this->setting['products3_title'] ?></p>
		</div>
		<div class="row">
			<div class="col-md-15">
				<div class="image"><img src="<?php echo Yii::app()->baseUrl.'/images/static/'. $this->setting['products3_itm_banner_1']; ?>" alt="" class="w-100"></div>
			</div>
			<div class="col-md-15">
				<div class="image"><img src="<?php echo Yii::app()->baseUrl.'/images/static/'. $this->setting['products3_itm_banner_2']; ?>" alt="" class="w-100"></div>
			</div>
			<div class="col-md-15">
				<div class="image"><img src="<?php echo Yii::app()->baseUrl.'/images/static/'. $this->setting['products3_itm_banner_3']; ?>" alt="" class="w-100"></div>
			</div>
			<div class="col-md-15">
				<div class="image"><img src="<?php echo Yii::app()->baseUrl.'/images/static/'. $this->setting['products3_itm_banner_4']; ?>" alt="" class="w-100"></div>
			</div>
		</div>
		<div class="bawah">
			<p>Trucks, Vessel, Floating Barges</p>
		</div>
	</div>
</section>
