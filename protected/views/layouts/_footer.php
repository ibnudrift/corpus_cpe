<section class="connect">
	<div class="prelative container">
		<div class="row">
			<div class="col-md-60">
				<div class="box-content">
					<p></p>
<!--					<a target="_blank" href="--><?php //echo $this->setting['url_facebook'] ?><!--"><img src="--><?php //echo $this->assetBaseurl; ?><!--facebook-logo-01.png" alt=""></a>-->
<!--					<a target="_blank" href="--><?php //echo $this->setting['url_instagram'] ?><!--"><img src="--><?php //echo $this->assetBaseurl; ?><!--facebook-logo-02.png" alt=""></a>-->
<!--					<a target="_blank" href="--><?php //echo $this->setting['url_youtube'] ?><!--"><img src="--><?php //echo $this->assetBaseurl; ?><!--facebook-logo-04.png" alt=""></a>-->
<!--					<a target="_blank" href="--><?php //echo $this->setting['url_linkedin'] ?><!--"><img src="--><?php //echo $this->assetBaseurl; ?><!--facebook-logo-03.png" alt=""></a>-->
				</div>
			</div>
		</div>
	</div>
</section>

<section class="footer">
	<div class="prelative container">
		<div class="section-box">
			<div class="row">
				<div class="col-md-20">
					<div class="row">
						<div class="col-md-60">
							<div class="box-footer">
								<div class="title">
									<p>Browse Corpus</p>
								</div>
								<div class="menu">
									<ul>
										<li><a href="<?php echo CHtml::normalizeUrl(array('/home/whatwedo', 'lang'=>Yii::app()->language)); ?>">Our Product</a></li>
										<li><a href="<?php echo CHtml::normalizeUrl(array('/home/about', 'category'=>'financial', 'lang'=>Yii::app()->language)); ?>">About Us</a></li>
										<li><a href="<?php echo CHtml::normalizeUrl(array('/home/part', 'lang'=>Yii::app()->language)); ?>">Our Partner</a></li>
										<li><a href="<?php echo CHtml::normalizeUrl(array('/home/career', 'lang'=>Yii::app()->language)); ?>">Career</a></li>
										<li><a href="<?php echo CHtml::normalizeUrl(array('/home/contactus', 'lang'=>Yii::app()->language)); ?>">Contact Us</a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col-md-40">
					<div class="box-footer">
						<div class="title">
							<p>Corpus Group</p>
						</div>
						<div class="image">
							<img class="img img-fluid" src="<?php echo $this->assetBaseurl; ?>footer-update_03.jpg" alt="">
						</div>
						<div class="content">
							<p>Our financial products are offered/underwritten by one or more of the following: Corpus Prima Mandiri; Corpus Sekuritas Indonesia, Corpus Kapital Manajemen, CPV, Corpus Prima Investama and operated in major cities in Indonesia - Surabaya, Jakarta, Bali, Malang, Medan, Semarang, Bandung.</p>
							
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- <div class="pb-5"></div> -->
		<div class="pb-5 d-none d-sm-block"></div>
		<div class="hr-footer"></div>
		<div class="pt-3"></div>
		<div class="pt-1"></div>
		<div class="row">
			<div class="col-md-20">
				<div class="footer-bawah">
					<img style="max-width:85px;" src="<?php echo $this->assetBaseurl; ?>corpus-header-foot.png" class="d-block pb-2">
					<!--					<p></p>-->
				</div>
			</div>
			<div class="col-md-40">
				<div class="footer-bawah">
					<p class="by">&copy; 2019 Corpus Group Indonesia - All rights reserved.<br>Website design by <a target="_blank" title="Website Design Surabaya" href="https://markdesign.net/">Mark Design</a>.</p>
				</div>
			</div>
		</div>
	</div>
	<div class="pb-5"></div>
</section>
